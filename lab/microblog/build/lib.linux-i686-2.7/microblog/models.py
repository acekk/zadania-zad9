# -*- coding: utf-8 -*-

from persistent.mapping import PersistentMapping
from persistent import Persistent
import datetime

class Blog(PersistentMapping):
    __name__ = None
    __parent__ = None



class Post(PersistentMapping):
    def __init__(self, title, content):
            super(Post, self).__init__()
            self.title = title
            self.content = content
            self.created = datetime.datetime.now()
    def __setitem__(self, name, item):
        super(Post, self).__setitem__(name, item)
        item.__name__ = name
        item.__parent__ = self

class Comment(Persistent):
    def __init__(self, author, message, tajtle):
        super(Comment, self).__init__()
        self.author = author
        self.message = message
        self.tajtle = tajtle

class SubComment(Persistent):
    def __init__(self, author, message):
        self.author = author
        self.message = message

def appmaker(zodb_root):
    if not 'app_root' in zodb_root:
        # Utworzenie głównegp kontenera
        app_root = Blog()
        zodb_root['app_root'] = app_root
        import transaction
        transaction.commit()
    return zodb_root['app_root']
