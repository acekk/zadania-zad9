# -*- coding: utf-8 -*-

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config
from pyramid.response import Response
from docutils.core import publish_parts
import time

import datetime
from .models import Blog, Post, Comment


# Główna strona
@view_config(context='.models.Blog', renderer='templates/list.pt')
def view_blog(context, request):
    """ Wyświetla listę n ostatnich wpisów """
    # sorted_blog_list= sorted(blog_list, key=lambda tup: tup[1].created, reverse=True)[:3]
    blog = context
    add_url = request.resource_url(context, 'add')
    return  dict(blog=blog, add_url=add_url)


@view_config(name='add_comment', context='.models.Post', renderer='templates/add_comment.pt')
def add_comment(context, request):
    add_url = request.resource_url(context, 'add_comment')
    comment = context.__parent__
    if request.method == 'POST':
        author = request.params['author']
        message = request.params['message']
        title = request.params['title']
        comment = Comment(author, message, title)
        comment.__name__ = title
        comment.__parent__ = context
        context[title] = comment
        return HTTPFound(location=request.resource_url(comment))
    page = Comment('', '', '')
    page.__name__ = 'Add new entry'
    page.__parent__ = context
    return dict(comment=comment, add_comment_url=add_url)

@view_config(context='.models.Post', renderer='templates/view.pt')
def view_post(context, request):
    content = publish_parts(context.content, writer_name='html')['html_body']
    add_comment_url = request.resource_url(context, 'add_comment')
    return dict(post=context, content=content, add_comment_url = add_comment_url)

@view_config(context='.models.Comment', renderer='templates/view_comment.pt')
def view_comment(context, request):
    return dict(comment=context)


@view_config(name='add', context='.models.Blog', renderer='templates/add_post.pt')
def add_post(context, request):
    add_url = request.resource_url(context, 'add')
    post = context.__parent__
    if request.method == 'POST':
        content = request.params['content']
        title = request.params['title']
        post = Post(title, content)
        post.__name__ = title
        post.__parent__ = context

        context[title] = post
        return HTTPFound(location=request.resource_url(post))
    page = Post('','')
    page.__name__ = 'Add new entry'
    page.__parent__ = context
    return dict(post=post, add_url=add_url)